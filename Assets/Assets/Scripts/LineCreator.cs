﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineCreator : MonoBehaviour
{
    public GameObject linePrefab;
    LineController activeLine;

    public static List<GameObject> LineList = new List<GameObject>();
    public GameObject[] currentLines;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (MainController.isGameStarted)
        {
            if (Input.GetMouseButtonDown(0))
            {
            
                GameObject lineCopy = Instantiate(linePrefab);
                activeLine = lineCopy.GetComponent<LineController>();
                LineList.Add(lineCopy);
                // Debug.Log(LineList.Count);
            }
            if (Input.GetMouseButtonUp(0))
            {
                if (LineList != null)
                {
                  //  ClearFalseLines();
                }

                
                activeLine = null;
                if (LineList.Count != 0)
                    StartCoroutine(DestroyWithDelay());

            }

            if (activeLine != null)
            {
                Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                activeLine.UpdateLine(mousePos);
            }
 

        }


        if (MainController.isGameOver)
        {
            activeLine = null;
        }

    }


    public IEnumerator DestroyWithDelay()
    {
        yield return new WaitForSeconds(2);
        List<GameObject> clearedLineList = LineList;
        if (clearedLineList.Count != 0)
        {
            Destroy(clearedLineList[0]);
            LineList.RemoveAt(0);
        }
    }

    public static void ClearFalseLines()
    {
        {
            //   Debug.Log("LineList.Count" + LineList.Count);
            if (LineList.Count != 0)
            {
                for (int i = 0; i < LineList.Count; i++)
                {
                    GameObject line = LineList[i];
                    int lineSize = line.GetComponent<LineRenderer>().positionCount;
                    //   Debug.Log(lineSize);

                    if (lineSize == 1)
                    {
                        Destroy(line);
                        LineList.Remove(line);

                    }
                }
            }
        }
    }
}

/*
         currentLines = GameObject.FindGameObjectsWithTag("Line");
        foreach (GameObject line in currentLines)
        {
            int lineSize = line.GetComponent<LineRenderer>().positionCount;
            Debug.Log(lineSize);

            if (lineSize == 1)
            {
                Destroy(line);
                LineList.Remove(line);

            }
        }
*/
