﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallController : MonoBehaviour
{
    public string edge = "Edge";
    public string obstacle = "Obstacle";
    public string line = "Line";
    public GameObject tryAgainButton;
    private Vector2 oldVelocity;
    public float constantSpeed = 1.0f;
    public float multiplier = 2.0f;
    public float smoothingFactor = 0.5f;
    public Rigidbody2D ballRigidBody;
    // Use this for initialization
    void Start()
    {
        ballRigidBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
       // Debug.Log("velocity before" + constantSpeed * ballRigidBody.velocity.normalized);
      //  Debug.Log("velocity11" + ballRigidBody.velocity);
        Vector3 dir = ballRigidBody.velocity.normalized;
        ballRigidBody.velocity = dir * constantSpeed* multiplier;
   //     ballRigidBody.velocity = Vector2.Lerp(ballRigidBody.velocity,
   //         constantSpeed * (ballRigidBody.velocity.normalized), Time.deltaTime * smoothingFactor); 
     //   Debug.Log("velocity after" + constantSpeed * (ballRigidBody.velocity.normalized));
     //   Debug.Log("velocity22" + ballRigidBody.velocity);

    }

    void FixedUpdate()
    {
        // because we want the velocity after physics, we put this in fixed update
        oldVelocity = this.GetComponent<Rigidbody2D>().velocity;
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
       // Debug.Log(collider.gameObject.name);
        if (collider.gameObject.tag == edge || collider.gameObject.tag == obstacle)
        {
            MainController.isGameOver = true;
            MainController.isGameStarted = false;

        }

    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        // get the point of contact
        ContactPoint2D contact = collision.contacts[0];
        // reflect our old velocity off the contact point's normal vector
        Vector3 reflectedVelocity = Vector3.Reflect(oldVelocity, contact.normal);
        // assign the reflected velocity back to the rigidbody
        this.GetComponent<Rigidbody2D>().velocity = reflectedVelocity;
      //  var cvel = this.GetComponent<Rigidbody2D>().velocity;
       // var tvel = cvel.normalized * constantSpeed;
     //   this.GetComponent<Rigidbody2D>().velocity = Vector3.Lerp(cvel, tvel, Time.deltaTime * smoothingFactor);
        // rotate the object by the same ammount we changed its velocity
        Quaternion rotation = Quaternion.FromToRotation(oldVelocity, reflectedVelocity);
        transform.rotation = rotation * transform.rotation;
        if (MainController.destroyOnCollision && collision.gameObject.tag == line)
        {
            Destroy(collision.gameObject);
        }
        MainController.bounceCount++;           
        
    }
}
