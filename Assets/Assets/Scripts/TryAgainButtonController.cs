﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TryAgainButtonController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (MainController.isGameStarted)
        {
            GameObject ball = GameObject.FindGameObjectWithTag("Ball");
            Transform ballTransform = ball.transform;
            Physics2D.IgnoreCollision(ballTransform.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
