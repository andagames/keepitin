﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LineController : MonoBehaviour
{

    public LineRenderer lineRenderer;
    public EdgeCollider2D edgeCol;
    public float currentLineLength=0;
    List<Vector2> points;

    public float lineLimit = 400.0f;
    public float lineLength = 0.0f;
    public bool limitReached = false;
    public static bool canInstentiate = false;
    public void UpdateLine(Vector2 mousePos)
    {
        if (points == null&& MainController.isGameStarted)
        {

            points = new List<Vector2>();
            SetPoint(mousePos);
            return;
        }

        if (Vector2.Distance(points.Last(), mousePos) > .1f && currentLineLength < 2 && MainController.isGameStarted)
        {
            //Debug.Log("lineLength : " + currentLineLength);
            if (currentLineLength < 0.7f)
            {
                SetPoint(mousePos);
            }
        
        }
    }

    void SetPoint(Vector2 point)
    {
        points.Add(point);
        lineRenderer.positionCount = points.Count;
        lineRenderer.SetPosition(points.Count - 1, point);
     
        if (points.Count > 1)
        {
            Vector2 line = new Vector2((points[points.Count-1].x - points[points.Count - 2].x), (points[points.Count-1].y - points[points.Count - 2].y));
            currentLineLength += line.magnitude; 
            edgeCol.points = points.ToArray();
      //      Debug.Log("currentLineLength:"+currentLineLength);
            MainController.UpdateLineLength(currentLineLength);
            canInstentiate = true;
        }
        else if(points.Count <= 1)
        {
            canInstentiate = false;

        }

        /*points.Add(point);
        if (lineLength <= lineLimit)
        {
            lineRenderer.positionCount = points.Count;
            lineRenderer.SetPosition(points.Count - 1, point);
            Vector2 line = new Vector2((points[points.Count].x - points[points.Count - 1].x), (points[points.Count].y - points[points.Count - 1].y));

            lineLength += line.magnitude;
            if (points.Count > 1)
            {

                Debug.Log("linelength+" + lineLength);
                edgeCol.points = points.ToArray();
            }
            else if (points.Count < 1)
            {
                Debug.Log("tek");
            }
        }*/

        /*
                Debug.Log("points.Count:" + points.Count);
                if (points.Count == 0)
                {
                    points.Add(point);
                    lineRenderer.positionCount = points.Count;
                    lineRenderer.SetPosition(points.Count - 1, point);          
                }
                else
                {

                    Vector2 line = new Vector2((point.x - points[points.Count-1].x), (point.y - points[points.Count-1].y));
                    //     Debug.Log("line.magnitude" + line.magnitude);
                    //  Debug.Log("lineLimit" + lineLimit);
                    //  Debug.Log("lineLength" + line.magnitude);
                    Debug.Log("limitReached" + limitReached);

                    if (!limitReached && (lineLength + line.magnitude) < lineLimit)
                    {
                        Debug.Log("girmiyorrr");

                        lineLength += line.magnitude;

                        points.Add(point);
                        lineRenderer.positionCount = points.Count;
                        lineRenderer.SetPosition(points.Count - 1, point);
                        edgeCol.points = points.ToArray();
                    }

                    else
                    {
                        limitReached = true;

                        /*
                        float diff = lineLimit - lineLength;

                        //endPoint = startPoint + (vector.normalized * length);

                        point = points[points.Count - 1] + (line.normalized * diff);

                        points.Add(point);
                        lineRenderer.positionCount = points.Count;
                        lineRenderer.SetPosition(points.Count - 1, point);

                        edgeCol.points = points.ToArray();
                        */
    }
}
   

