﻿using GooglePlayGames;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

public class MainController : MonoBehaviour
{
    public GameObject ballPrefab;
    public GameObject obstaclePrefab;

    public GameObject timerText;
    public GameObject bounceText;
    public GameObject tryAgainButton;
    public GameObject collisionModeButton;
    public GameObject playerPointText;
    public GameObject lengthText;
    public GameObject highScoreText;
    public GameObject leaderBoardButton;
    public GameObject SocialButton;

    public float deltaX;
    public float deltay;

    private int seconds;
    private int minutes;

    private int playerPoint;

    private bool isDrawing = false;
    private bool isTrailCleared = true;
    private bool isInstentiated = false;
    public static bool isGameOver = false;
    public static bool isGameStarted = false;
    public static bool destroyOnCollision = false;
    public Vector2 inputPosition;
    public Vector2 tempVector;
    public static float totalLineLength = 0;
    public int totalLineLimit = 500;
    public static int bounceCount = 0;
    public GameObject[] currentLines;
    public GameObject[] currentBalls;
    public GameObject[] currentObstacles;
    public GameObject currentBall;
    public bool isBallCreated = false;
    public bool canAmplify = true;
    // Use this for initialization
    void Start()
    {


        collisionModeButton.GetComponent<Text>().text = "Collision Mode: Off";
        collisionModeButton.GetComponent<Text>().color = new Color32(146, 255, 255, 255);


        GenerateBall();
        StartObstacleGeneration();
        playerPointText.SetActive(false);
        tryAgainButton.SetActive(false);
        tryAgainButton.GetComponent<Collider2D>().enabled = false;
        StartTimer();


    }

    // Update is called once per frame
    void Update()
    {


        if (isGameOver)
        {
            GameOver();
        }



        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit.collider != null)
        {
            if (Input.GetMouseButtonDown(0))
            {
                //    Debug.Log("current hit: " + hit.collider.name);
                if (hit.collider.name.Equals("TryAgainButton"))
                {
                    RestartGame();
                }
                else if (hit.collider.name.Equals("CollisionModeButton"))
                {
                    if (destroyOnCollision == false)
                    {
                        destroyOnCollision = true;
                        collisionModeButton.GetComponent<Text>().text = "Collision Mode: On";
                        collisionModeButton.GetComponent<Text>().color = new Color32(255, 156, 144, 255);
                    }
                    else if(destroyOnCollision == true)
                    {
                        destroyOnCollision = false;
                        collisionModeButton.GetComponent<Text>().text = "Collision Mode: Off";
                        collisionModeButton.GetComponent<Text>().color = new Color32(146, 255, 255, 255);

                    }
                }
                else if (hit.collider.name.Equals("LeaderBoard"))
                {
                    Debug.Log("LeaderBoard");

                    PlayGamesPlatform.Activate();
                    Social.localUser.Authenticate((bool success) => {
                        // handle success or failure
                    });
                    Social.ReportScore(PlayerPrefs.GetInt("highScoreKey"), "CgkIjMDHrtoaEAIQBQ", (bool success) => {
                    });
                    Social.ShowLeaderboardUI();

                    ILeaderboard lb = PlayGamesPlatform.Instance.CreateLeaderboard();
                    lb.id = "CgkIjMDHrtoaEAIQBQ";
                    lb.LoadScores(ok =>
                    {
                        if (ok)
                        {
                            LoadUsersAndDisplay(lb);
                        }
                        else
                        {
                            Debug.Log("Error retrieving leaderboardi");
                        }
                    });

                    LoadUsersAndDisplay(lb);

                }
                else if (hit.collider.name.Equals("Achievements"))
                {
                    Debug.Log("Achievements");
                    PlayGamesPlatform.Activate();
                    Social.ShowAchievementsUI();

                    Social.localUser.Authenticate((bool success) => {
                        // handle success or failure
                        if (success)
                        {
                            Social.ShowAchievementsUI();

                        }
                        else
                        {

                        }

                    });

                }

            }
        }


        SowRemainingLength();
        DestroSurplusLines();
        PrintBounceCounts();
     //   AddVelocityToBall();

    }

    private void AddVelocityToBall()
    {
        currentBall = GameObject.FindGameObjectWithTag("Ball");
       // Debug.Log("hız" + currentBall.GetComponent<Rigidbody2D>().velocity.x + ",  " + currentBall.GetComponent<Rigidbody2D>().velocity.y);



        if (isGameStarted && !isGameOver && currentBall != null 
            && (currentBall.GetComponent<Rigidbody2D>().velocity.x < 0.1f
            && currentBall.GetComponent<Rigidbody2D>().velocity.y < 0.1f)
            ||( currentBall.GetComponent<Rigidbody2D>().velocity.x > -0.1f
             && currentBall.GetComponent<Rigidbody2D>().velocity.y > -0.1f))            
        {
          //  Debug.Log("Velocityyyyyyyy" + currentBall.GetComponent<Rigidbody2D>().velocity.x+ ",   "+ currentBall.GetComponent<Rigidbody2D>().velocity.y);
            Vector2 amplifiedVector = new Vector2(currentBall.GetComponent<Rigidbody2D>().velocity.x*10, currentBall.GetComponent<Rigidbody2D>().velocity.y*10);
            Vector2 maxVector;
            if (currentBall.GetComponent<Rigidbody2D>().velocity.x > 1f || currentBall.GetComponent<Rigidbody2D>().velocity.x < -1f)
            {
                maxVector = new Vector2(1f, currentBall.GetComponent<Rigidbody2D>().velocity.y);
                currentBall.GetComponent<Rigidbody2D>().AddForce(maxVector);

            }
            else if (currentBall.GetComponent<Rigidbody2D>().velocity.y > 1f || currentBall.GetComponent<Rigidbody2D>().velocity.y < -1f)
            {
                maxVector = new Vector2(currentBall.GetComponent<Rigidbody2D>().velocity.x, 1f);
                currentBall.GetComponent<Rigidbody2D>().AddForce(maxVector);

            }
            else
            {
                currentBall.GetComponent<Rigidbody2D>().AddForce(amplifiedVector);
            }

        }
        

    }

    private void DestroSurplusLines()
    {
        currentLines = GameObject.FindGameObjectsWithTag("Line");
        if (currentLines.Length > 2)
        {
            Destroy(currentLines[0]);
        }
    }

    public void StartTimer()
    {
        isGameStarted = true;
        StartCoroutine(Timer());
    }

    public void StartObstacleGeneration()
    {
        StartCoroutine(GenerateObstacle());
    }
    public void ResetTimer()
    {
        StopAllCoroutines();

        seconds = 0;
        minutes = 0;
        timerText.gameObject.GetComponent<Text>().text = minutes.ToString() + ":" + seconds.ToString();
    }
    public void ResetBounceCount()
    {
        bounceCount = 0;
        bounceText.GetComponent<Text>().text = "0";

    }
    public void ResetLineLimit()
    {

    }
    public void ResetLineDuration()
    {

    }
    public void PrintBounceCounts()
    {
        bounceText.GetComponent<Text>().text = bounceCount.ToString();
    }
    public static void UpdateLineLength(float currentLineLength)
    {
        totalLineLength += currentLineLength;
        //    Debug.Log("totalLineLength: " + totalLineLength);
    }
    public void ClearLines()
    {
        currentLines = GameObject.FindGameObjectsWithTag("Line");
        foreach (GameObject line in currentLines)
        {
            Destroy(line);
        }
    }
    public void ClearBalls()
    {
        currentBalls = GameObject.FindGameObjectsWithTag("Ball");
        foreach (GameObject ball in currentBalls)
        {
            Destroy(ball);
        }
    }
    public void ClearObstacles()
    {
        currentObstacles = GameObject.FindGameObjectsWithTag("ParentObstacle");
        foreach (GameObject obstacle in currentObstacles)
        {
            Destroy(obstacle);
        }


    }
    public void GameOver()
    {
        StopAllCoroutines();
        ShowAd();
        // ConvertTimeToPoints();
        ConvertBouncesToPoint();
        tryAgainButton.SetActive(true);
        tryAgainButton.GetComponent<Collider2D>().enabled = true;
        GameObject tempBall = GameObject.FindGameObjectWithTag("Ball");
        tempBall.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
        playerPointText.SetActive(true);
    }
    public void RestartGame()
    {
        ClearLines();
        ClearBalls();
        ClearObstacles();
        ResetTimer();
        ResetBounceCount();
        ResetRemainingLength();
        ResetLineLimit();
        ResetLineDuration();
        playerPointText.SetActive(false);
        tryAgainButton.SetActive(false);
        tryAgainButton.GetComponent<Collider2D>().enabled = false;
        isGameOver = false;
        isGameStarted = true;
        StartTimer();
        GenerateBall();
        StartObstacleGeneration();
        currentLines = GameObject.FindGameObjectsWithTag("Line");
        foreach (GameObject line in currentLines)
        {
            int size = line.GetComponent<LineRenderer>().positionCount;
            if (size == 1)
            {
                Destroy(line);
            }
        }
    }


    public void ConvertTimeToPoints()
    {
        playerPoint = (seconds * 5) + (minutes * 300);
        playerPointText.GetComponent<Text>().text = playerPoint.ToString();
    }
    
        public void ConvertBouncesToPoint()
    {
        playerPoint = bounceCount;
        playerPointText.GetComponent<Text>().text = playerPoint.ToString();
    }
    public void GenerateBall()
    {
       // Debug.Log("GenerateBall");
        GameObject ballClone = Instantiate(ballPrefab) as GameObject;
        ballClone.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
        float randomX = UnityEngine.Random.Range(-70f, 70f);
        float randomY = 70f;
        //  float randomY = Mathf.Sqrt(70 - (randomX * randomX));
        tempVector = new Vector2(randomX, randomY);
     //   Debug.Log("tempvector" + randomX + ", " + randomY);

        ballClone.gameObject.GetComponent<Rigidbody2D>().AddRelativeForce(tempVector);
        // ballClone.gameObject.GetComponent<Rigidbody2D>().velocity = tempVector;
        isBallCreated = true;
        currentBall = GameObject.FindGameObjectWithTag("Ball");


    }
   
    public IEnumerator GenerateObstacle()
    {
        while (true)
        {
            GameObject obstacleClone = Instantiate(obstaclePrefab) as GameObject;
            obstacleClone.transform.SetParent(this.transform);
            obstacleClone.transform.localPosition = new Vector3(0f, 330f, 0f);
            obstacleClone.transform.localScale = new Vector3(1f, 1f, 1f);
            yield return new WaitForSeconds(7.5f);
        }
    }
    public void SowRemainingLength()
    {
        int remainingLength = totalLineLimit - Mathf.RoundToInt(totalLineLength);
        lengthText.GetComponent<Text>().text = remainingLength.ToString();
    }
    public void ResetRemainingLength()
    {
        totalLineLength = 0;
        lengthText.GetComponent<Text>().text = "0";
    }
    public IEnumerator Timer()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            seconds++;
            if (seconds == 60)
            {
                minutes++;
                seconds = 0;
            }
            if (seconds < 10 && minutes < 10)
            {
                timerText.gameObject.GetComponent<Text>().text = "0" + minutes.ToString() + " : 0" + seconds.ToString();
            }
            else if (seconds < 10 && minutes > 9)
            {
                timerText.gameObject.GetComponent<Text>().text = minutes.ToString() + " : 0" + seconds.ToString();
            }
            else if (minutes < 10 && seconds > 9)
            {
                timerText.gameObject.GetComponent<Text>().text = "0" + minutes.ToString() + " : " + seconds.ToString();
            }
            else
            {
                timerText.gameObject.GetComponent<Text>().text = minutes.ToString() + " : " + seconds.ToString();
            }
        }
    }

    internal void LoadUsersAndDisplay(ILeaderboard lb)
    {
        // get the user ids
        List<string> userIds = new List<string>();

        foreach (IScore score in lb.scores)
        {
            userIds.Add(score.userID);
        }
        // load the profiles and display (or in this case, log)
        Social.LoadUsers(userIds.ToArray(), (users) =>
        {
            string status = "Leaderboard loading: " + lb.title + " count = " +
                lb.scores.Length;
            foreach (IScore score in lb.scores)
            {
                IUserProfile user = FindUser(users, score.userID);
                status += "\n" + score.formattedValue + " by " +
                    (string)(
                        (user != null) ? user.userName : "**unk_" + score.userID + "**");
            }
            Debug.Log(status);
        });
    }
    private IUserProfile FindUser(IUserProfile[] users, string userID)
    {
        throw new NotImplementedException();
    }

    public void ShowAd(string zone = "")
    {
#if UNITY_EDITOR
        Debug.Log("Kardeş buraya giriyon mu bari");
        StartCoroutine(WaitForAd());

#endif

        if (string.Equals(zone, ""))
            zone = null;

        ShowOptions options = new ShowOptions();
        options.resultCallback = AdCallbackhandler;

        if (Advertisement.IsReady(zone))
        {
            Debug.Log("Peki Ya burya ????");
            Advertisement.Show(zone, options);

        }
    }

    void AdCallbackhandler(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:

                Debug.Log("Reklam izlendi");
                PlayerPrefs.SetInt("life", (PlayerPrefs.GetInt("life") + 1));
                break;
            case ShowResult.Skipped:
                Debug.Log("geçme birader");

                break;
            case ShowResult.Failed:
                Debug.Log("reklamı izlemekte başarısız oldun, bravo..");

                break;
        }
    }
    IEnumerator WaitForAd()
    {
        float currentTimeScale = Time.timeScale;
        Time.timeScale = 0f;

        yield return null;

        while (Advertisement.isShowing)
            yield return null;

        Time.timeScale = currentTimeScale;
    }
}
