﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms;

public class LoginController : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()

         .Build();

        PlayGamesPlatform.InitializeInstance(config);
        // recommended for debugging:
        PlayGamesPlatform.DebugLogEnabled = true;
        // Activate the Google Play Games platform
        PlayGamesPlatform.Activate();
        if (!PlayGamesPlatform.Instance.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.Authenticate((bool success) =>
            {
                if (success)
                {
                    Debug.Log("You've successfully logged in");

                }
                else
                {
                    Debug.Log("Login failed for some reason");

                }
            }, true);
        }
        if (PlayerPrefs.GetInt("isLogged") == 0)
        {
        }

        else if (PlayerPrefs.GetInt("isLogged") == 1)
        {
        }

    }


    void ProcessAuthentiation(bool success)
    {
        if (success)
        {
            Debug.Log("You've successfully logged in");

        }
        else
        {
            Debug.Log("Login failed for some reason");
        }
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit.collider != null)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (hit.collider.name.Equals("LoginButton"))
                {
                    SceneManager.LoadScene("GameScene");
                }
            }
        }
    }

   

   

}